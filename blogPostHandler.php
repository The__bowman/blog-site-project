<?php
//Performs interaction with database to submit post
//to be added to database
//Author: Dustin Johnson
require "funcs.php";
session_start();

// check if user has been logged in
if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true){


$con = dbConnect();

// store data from form
$title = $_POST['title'];
$postBody = $_POST['body'];
$author = $_SESSION['username'];
$user_id = $_SESSION['userID'];
$date = date("y-m-d");

// checks for empty title or post
if (empty($title) || empty($postBody)){
    echo "<body style=\"background-color: red; text-align: center\">";
    echo "<h3 style=\"color: white;\">" . "Field left empty. Please ensure all fields are filled out.</h3>";
    echo "</body>";
    exit();
}

// checks for blocked words, returns a boolean
if (!filterwords($postBody)){

$sql = "INSERT INTO posts (USER_ID, TITLE, BODY, AUTHOR, CREATED_AT, users_ID) VALUES ('$user_id', '$title', '$postBody', '$author', '$date', '$user_id');";

$result = mysqli_query($con, $sql);
if ($result){
  mysqli_close($con);
  header('location: index.php?=post_successful');
}

}
// stops post from being created with blocked words
else {
    echo "<body style=\"background-color: red; text-align: center\">";
    echo "<h3 style=\"color: white;\">" . "Post contained banned language.</h3>";
    echo "</body>";
    exit();
}
}
