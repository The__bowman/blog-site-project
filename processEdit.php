<?php
// proccesses and performs SQL query, handles database interaction
// author: Dustin Johnson
require "funcs.php";
session_start();
//verifies user is admin
if (!isAdmin()){
  header('location: index.php');
}
// check if user has been logged in
if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true){



$con = dbConnect();

// store data from form
$title = $_POST['title'];
$postBody = $_POST['body'];
$author = $_SESSION['username'];
$user_id = $_SESSION['userID'];
$date = date("y-m-d");
$id = $_POST['id'];

// checks for empty title or post
if (empty($title) || empty($postBody)){
    header("Location: .../adminPageHandler.php?error=emptyfields");
    exit();
}

// checks for blocked words, returns a boolean
if (!filterwords($postBody)){

$sql = "UPDATE posts SET TITLE = '$title', BODY = '$postBody' WHERE POST_ID = '$id';";
$result = mysqli_query($con, $sql);
if ($result){
  echo "Post Updated<br>";
  echo "<a href=index.php>CLICK HERE TO RETURN TO MAIN PAGE</a>";
}
mysqli_close($con);

}
// stops post from being created with blocked words
else {
    echo "Post contains banned language";
    exit();
}
}
// stops post from being created without being logged in
else {
    echo "User not logged in";
    exit();
}
