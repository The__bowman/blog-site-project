<?php
//functions file
//Author: Dustin Johnson
define('USER_LEVEL_ADMIN', 1);

function dbConnect() {
    $user = 'root';
    $password = 'root';
    $db = 'milestone';
    $host = 'localhost';

    $con = mysqli_connect($host, $user, $password, $db);

    if (!$con){
        die("Connection failed: " . mysqli_connect_error());
    }
    return $con;
}
// creates an array of strings from the post to be authored
// performs a simple comparison of each word against blocked
// words
function filterwords($text){
   $flag = false; // flag used to identify if a blocked word has been detected
   $word = '';
   $check = explode(" ", $text); // returns an array of strings, split on a space
   foreach ($word as $check){
       switch ($word){
           case 'fuck':
               $flag = true;
               break;
           case 'shit':
               $flag = true;
               break;
           case 'bitch':
               $flag = true;
               break;
           case 'cock':
               $flag = true;
               break;
           case 'cunt':
               $flag = true;
               break;
           case 'clit':
               $flag = true;
               break;
           case 'faggot':
               $flag = true;
               break;
           case 'nigger':
               $flag = true;
               break;
           case 'nigga':
               $flag = true;
               break;
       }
   }
   return $flag;
}

function isAdmin() {
    if (isset($_SESSION['username']) && USER_LEVEL_ADMIN == $_SESSION['level'])
        return true;
    else
        return false;

}

function getAllUsers(){
  $con = dbConnect();
  $index = 0;
  $sql = "SELECT * FROM users WHERE 1;";
  $result = mysqli_query($con, $sql);
  $resultCheck = mysqli_num_rows($result);
  if ($resultCheck > 0){
    while ($row = mysqli_fetch_assoc($result)){
      $users[$index] = array($row["ID"], $row["EMAIL"], $row["USERNAME"], $row["PLATFORM"],
                            $row["DB_PASSWORD"], $row["USER_LVL"]);
      $index++;
    }
  }
  mysqli_close($con);
  return $users;
}

function getAllPosts(){
    $con = dbConnect();
    $index = 0;
    $sql = "SELECT ID, TITLE, AUTHOR FROM posts;";
    $result = mysqli_query($con, $sql);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck > 0){
        while ($row = mysqli_fetch_assoc($result)){
            $posts[$index] = array($row["ID"], $row["TITLE"], $row["AUTHOR"]);
            ++$index;
        }
    }
    mysqli_close($con);
    return $posts;

}

function getPostbyID($postID){
  $con = dbConnect();
  $index = 0;
  $sql = "SELECT * FROM posts WHERE POST_ID = '$postID' LIMIT 1;";
  $result = mysqli_query($con, $sql);
  $resultCheck = mysqli_num_rows($result);

  if ($resultCheck > 0){
      while ($row = mysqli_fetch_assoc($result)){
          $posts[$index] = array($row["POST_ID"], $row["USER_ID"], $row["AUTHOR"],
                            $row["TITLE"], $row["BODY"], $row["CREATED_AT"]);
          ++$index;
      }
  }
  mysqli_close($con);
  return $posts;
}

function getPostbyTitle($postTitle){
  $con = dbConnect();
  $index = 0;
  $sql = "SELECT * FROM posts WHERE POST_ID LIKE '%$postTitle%';";
  $result = mysqli_query($con, $sql);
  $resultCheck = mysqli_num_rows($result);

  if ($resultCheck > 0){
      while ($row = mysqli_fetch_assoc($result)){
          $posts[$index] = array($row["POST_ID"], $row["USER_ID"], $row["AUTHOR"],
                            $row["TITLE"], $row["BODY"], $row["CREATED_AT"]);
          ++$index;
      }
  }
  mysqli_close($con);
  return $posts;
}

function deletePost($postID){
  $con = dbConnect();
  $sql = "DELETE FROM posts WHERE POST_ID LIKE '$postID';";
  $result = mysqli_query($con, $sql);

  $check = "SELECT * FROM posts WHERE POST_ID LIKE '$postID';";
  $resultCheck = mysqli_query($con, $check);
  if ($resultCheck == null){
    return true;
  } else {
    return false;
  }
}

function updatePost($text, $postID){
  $con = dbConnect();
  $sql = "UPDATE posts SET BODY = $text WHERE POST_ID LIKE $postID;";
  $result = mysqli_query($con, $sql);
}
