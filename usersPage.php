<html>
<head>
	<style>
	   body{
	       background: linear-gradient( to left, rgba(255, 255, 255, 1), rgba(0, 0, 255, 1));
	   }
	</style>
</head>
<body>
<?php
/*
* Displays posts with buttons to handle editing
* and deleting posts
* author: Dustin Johnson
*/
include 'funcs.php';
session_start();
if (!isAdmin()){
  header('location: index.php');
}
$con = dbConnect();


$sql = "SELECT * FROM users WHERE 1;";
$result = mysqli_query($con, $sql);
$resultCheck = mysqli_num_rows($result);
if ($resultCheck > 0){
    while ($row = mysqli_fetch_assoc($result)){
        echo "<font color=\"cyan\">ID: " . $row['ID'] . "<br>";
				echo "E-Mail: " . $row['EMAIL'] . "<br>";
        echo "Username: " . $row['USERNAME'] . "<br>";
        echo "Platform: " . $row['PLATFORM'] . "<br>";
        echo "Admin: " . $row['USER_LVL'] . "<br><br></font>";
				?>
				<form action="processDeleteUser.php" method="post">
					<input type="hidden" name="id" value="<?php echo $row['ID']; ?>"></input>
					<button id="del_btn" onclick="confirm()" type="submit">Delete</button>
          <script>
            function confirm(){
              confirm("Are you sure you want to delete?\nThis is permanent and cannot be undone.");
            }
          </script>
				</form>
				<form action="editUserForm.php" method="post">
					<input type="hidden" name="id" value="<?php echo $row['ID']; ?>"></input>
					<button type="submit">Edit</button>
				</form>
        <p style="color: cyan;">Back to <a href="adminPage.php">Admin</a></p>

       <?php  echo "<font color=\"cyan\">*****************************" . "<br><br></font>";

    }
}
 ?>
</body>
</html>
