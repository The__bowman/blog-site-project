<?php
session_start();
if (!isset($_SESSION['loggedIn'])){
  header('location: index.php?=sign_in_required');
}
 ?>
 <html>
 <head>
   <style>
      body {
        background: black;
          background-image: url("wokenthehive.jpg");
          background-repeat:  no-repeat;
          background-attachment: fixed;
          background-position: center;
          background-size: contain;
      }
   </style>
   <title>Destiny Bloggers</title>
 </head>
<link rel="stylesheet" href="stylesheet.css">
<body>
  <form align="center" action="blogPostHandler.php" method="POST" >
    <p style="color: white;"> Title: </p>
    <input type="text" name="title" /> <br><br>
    <label style="color: white;" for="body">Main Body: </label>
    <textarea rows="10" cols="40" maxlength="400" name="body"></textarea><br><br>
    <p style="color: white;">Posts have a character limit of 400 characters</p>
    <input style="width: 100px;" class="buttons" type="submit" value="Post" /> <br>
  </form>
</body>
</html>
