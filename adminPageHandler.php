<html>
<head>
	<style>
	   body{
	       background: linear-gradient( to right, rgba(255, 255, 255, 1), rgba(0, 0, 255, 1));
	   }
	</style>
</head>
<body>
<?php
/*
* Displays posts with buttons to handle editing
* and deleting posts
* author: Dustin Johnson
*/
include 'funcs.php';
session_start();
if (!isAdmin()){
  header('location: index.php');
}
$con = dbConnect();


$sql = "SELECT * FROM posts WHERE 1;";
$result = mysqli_query($con, $sql);
$resultCheck = mysqli_num_rows($result);
if ($resultCheck > 0){
    while ($row = mysqli_fetch_assoc($result)){
        echo "Post ID: " . $row['POST_ID'] . "<br>";
				echo "User ID: " . $row['USER_ID'] . "<br>";
        echo "Author: " . $row['AUTHOR'] . "<br>";
        echo "Title: " . $row['TITLE'] . "<br>";
        echo "Body: " . "<pre>" . $row['BODY'] . "</pre><br>";
        echo "Date Created: " . $row['CREATED_AT'] . "<br><br>";
				?>
				<form action="processDeletion.php" method="post">
					<input type="hidden" name="id" value="<?php echo $row['POST_ID']; ?>"></input>
					<button id="del_btn" onclick="confirm()" type="submit">Delete</button>

				</form>
				<form action="editForm.php" method="post">
					<input type="hidden" name="id" value="<?php echo $row['POST_ID']; ?>"></input>
					<button type="submit">Edit</button>
				</form>
        <p>Back to <a href="adminPage.php">Admin</a></p>
        <script>
          function confirm(){
            confirm("Are you sure you want to delete?\nThis is permanent and cannot be undone.");
          }
        </script>
       <?php  echo "*****************************" . "<br><br>";

    }
}
 ?>
</body>
</html>
