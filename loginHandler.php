<?php
//Logs user in to site verifying against database and params
//Author: Dustin Johsnon
session_start();
include 'funcs.php';
//obtain data from form
$username = $_POST['username'];
$pwd = $_POST['password'];

//check for empty variables
if (empty($username) || empty($pwd)){
    echo "<body style=\"background-color: red; text-align: center\">";
    echo "<h3 style=\"color: white;\">" . "Field left empty. Please ensure all fields are filled out.</h3>";
    echo "</body>";
    exit();
}
else {
    //if variables are not empty, connect to database
   $con = dbConnect();

    //prepare the statement, use ? placeholder for security purposes
    $sql = "SELECT * FROM users WHERE USERNAME=?;";
    $stmt = mysqli_stmt_init($con);

    // prepares statement and checks if it will be capable of getting a result
    if (!mysqli_stmt_prepare($stmt, $sql)){
        echo 'SQL ERROR';
        close($con);
        exit();
    }
    else {
        //binds statement into $stmt variable, executes and stores result
        mysqli_stmt_bind_param($stmt, "s", $username);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);

        //checks result exists, if it doesn't writes no user found to screen
        if ($row = mysqli_fetch_assoc($result)){

            // checks password entered matches database
            if ($pwd == $row['DB_PASSWORD']){
                session_start();
                $_SESSION['userID'] = $row['ID'];
                $_SESSION['username'] = $row['USERNAME'];
                $_SESSION['level'] = $row['USER_LVL'];
                $_SESSION['loggedIn'] = true;

                /*echo 'Login Success!!!<br>';
                echo $_SESSION['level'] . '<br>';
                echo "<a href=index.php>Main Page</a><br>";
                if (isAdmin()){
                echo "<a href=adminPage.php>Admin Page</a>";
              } */
              header('location: index.php?=login_success');
                close($con);
                exit();
            }
            else {
                echo 'INCORRECT PASSWORD <br>';
                close($con);
                exit();
            }
        }
        else{
            echo'NO USER FOUND';
            close($con);
            exit();
        }
    }

}
