<?php
//connects to database
require "funcs.php";
$con = dbConnect();

// store data from form
$email = $_POST['email'];
$username = $_POST['username'];
$platform = $_POST['platform'];
$pwd = $_POST['password'];

// ensures all data required is not empty
if (empty($email) || empty($username) || empty($platform) || empty($pwd)){
  echo "<body style=\"background-color: red; text-align: center\">";
  echo "<h3 style=\"color: white;\">" . "Field left empty. Please ensure all fields are filled out.</h3>";
  echo "<a href=registration.html> Click here to try again </a>";
  echo "</body>";
}

$check = "SELECT * FROM users WHERE USERNAME = '$username';";
$result = mysqli_query($con, $check);
$count = mysqli_num_rows($result);

// checks for validity
if (!$result){
  throw new Exception('Could not execute query');
}
// checks for unique username
if ($count > 0){
    echo "<body style=\"background-color: red; text-align: center\">";
    echo "<h3 style=\"color: white;\"> Username taken. Please use a different one.</h3>";
    echo "<a href=registration.html> Click here to try again</a>";
    exit();
}
// executes adding new user to database
$sql = "INSERT INTO `users`(`EMAIL`, `USERNAME`, `PLATFORM`, `DB_PASSWORD`)  VALUES ('$email', '$username', '$platform', '$pwd')";

// perform registration
mysqli_query($con, $sql);

// get user ID to store in session after successful registration
$getid = "SELECT ID FROM users WHERE USERNAME = '$username';";
$id = mysqli_query($con, $getid);

// get user lvl to store in session after successful registration
$getLVL = "SELECT USER_LVL FROM users WHERE USERNAME = '$username';";
$lvl = mysqli_query($con, $getlvl);

// log new user in for first time
session_start();
$_SESSION['userID'] = $id;
$_SESSION['username'] = $username;
$_SESSION['level'] = $row['USER_LVL'];
$_SESSION['loggedIn'] = true;

 // send user back to main page
header('location: index.php?=registration_success');

close($con);


?>
