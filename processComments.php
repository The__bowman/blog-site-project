
<?php
// Interacts with database to post comments
// Author Dustin Johnson
require "funcs.php";
session_start();

// check if user has been logged in
if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {

    $con = dbConnect();

    // store data from comment form
    $comment = $_POST['comment'];
    $id = $_POST['id'];
    $user_id = $_SESSION['userID'];
    $rate = $_POST['rating'];
    // checks for empty title or post
    if (empty($comment)) {
        header("Location: .../processComments.php?error=emptyfields");
        exit();
    }

    // checks for blocked words, returns a boolean
    if (!filterwords($comment)) {
        if (empty($rate)) {
          // if rating is left empty, it is not a required field
            $sql = "INSERT INTO comments_table(comment_text, posts_POST_ID, users_ID) VALUES ('$comment', '$id', '$user_id');";

            $result = mysqli_query($con, $sql);
            if ($result) {
              //close connection and display success message in url
                header('location: allposts.php?=comment_successful');
                mysqli_close($con);
            }
        } else {
          // if rating is not empty
            $sql = "INSERT INTO comments_table(comment_text, rating, posts_POST_ID, users_ID) VALUES ('$comment', '$rate', '$id', '$user_id');";

            $result = mysqli_query($con, $sql);
            if ($result) {
              // close db connection and display success message in url
                mysqli_close($con);
                header('location: allposts.php?=comment_successful');
            }
        }
    }  else {
        // stops post from containing banned words
            echo "Post contains banned language";
            exit();
    }
} else {
    // stops post from being created without being logged in
    echo "User not logged in";
    exit();
}
