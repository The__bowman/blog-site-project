<?php
// page connects to db and fills edit form with the data from the post
// author: Dustin Johnson
session_start();
include 'funcs.php';
if (!isAdmin()){
  header('location: index.php?=admins_only');
}
$con = dbConnect();
$id = $_POST['id'];
$sql = "SELECT * FROM users WHERE ID = '$id';";
$result = mysqli_query($con, $sql);
if ($result){
  while ($row = mysqli_fetch_assoc($result)) {
    $email = $row['EMAIL'];
    $username = $row['USERNAME'];
    $platform = $row['PLATFORM'];
    $password = $row['DB_PASSWORD'];
    $admin = $row['USER_LVL'];
//    echo $email . " " . $username . " " . $platform . " " . $password . " " . $admin;
  }
} else {
    echo "something isn't right";
}
 ?>
<html>
<head>
  <style>
    html{
      background: url("destinyguardians.jpg");
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      background-attachment: fixed;
    }
    p {
      color: yellow;
    }
  </style>
	<title>>Destiny Bloggers</title>
</head>
<link rel="stylesheet" href="stylesheet.css">
<body class="mainbkgrnd">
	<h3 style="color: yellow; text-align: center;">Edit User</h3>
  <form align="center" action="processEditUser.php" method="POST" >
    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
    <p class="makeBold"> Enter E-Mail: </p>
    <input type="text" name="email" value="<?php echo $email; ?>"/><br><br>
    <p class="makeBold"> Enter Username: </p>
    <input type="text" name="username" value=" <?php echo $username; ?> "/><br><br>
    <p class="makeBold"> Enter Platform: </p>
    <input type="text" name="platform" value=" <?php echo $platform; ?>"/><br><br>
    <p class="makeBold"> Enter Password: </p>
    <input type="text" name="password" value=" <?php echo $password; ?> "/><br><br>
    <p class="makeBold"> Admin: </p>
    <input type="text" name="admin" value=" <?php echo $admin; ?> "/><br><br>
    <input style="background-color: rgba(255, 255, 0, 1);
                  width: 183px;"
                  type="submit" value="Edit User" /><br>
  </form>
</body>
</html>
