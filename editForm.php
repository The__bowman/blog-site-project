<?php
// page connects to db and fills edit form with the data from the post
// author: Dustin Johnson
session_start();
include 'funcs.php';
if (!isAdmin() && $_SESSION['userID'] != $_POST['user']){
  header("Location: .../editForm.php");
  exit();
}
$con = dbConnect();
$id = $_POST['id'];
$sql = "SELECT * FROM posts WHERE POST_ID = '$id';";
$result = mysqli_query($con, $sql);
if ($result){
  while ($row = mysqli_fetch_assoc($result)) {
    $title = $row["TITLE"];
    $body = $row["BODY"];
  }
} else {
    echo "something isn't right";
}
 ?>
<html>
<head>
  <style>
  html{
    background-color: #14EF23;
  }
  </style>
	<title>>Destiny Bloggers</title>
</head>
<link rel="stylesheet" href="stylesheet.css">
<body>
	<h3 align="center">Edit Post</h3>
		<form align="center" action="processEdit.php" method="POST" >
      <!-- hide id value but maintain it to be sent with the form -->
      <input type="hidden" name="id" value="<?php echo $id; ?>"/>
			<p> Title: </p>
			<input type="text" name="title" value="<?php echo $title; ?>"/> <br><br>
			<label for=body>Main Body: </label>
			<textarea rows="10" cols="40" maxlength="400" name="body"><?php echo $body; ?>
			</textarea><br><br>
			<p>Posts have a character limit of 400 characters</p>
			<input type="submit" value="Edit" /> <br>
		</form>
</body>
</html>
